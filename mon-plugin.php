<?php
/*
Plugin Name: Mon plugin
Plugin URI: https://site-du-plugin.fr
Description: Ce plugin WordPress sert à …
Author: Moi
Version: 1.0
Author URI: https://mon-site.fr
*/


// Création du cpt apprenants

add_theme_support( 'post-thumbnails' ); // permet de add l'image mis en avant sur wp (il faut add 'thumbnail' dans 'supports' plus bas également)

function monplugin_custom_post_types() {
    $cptapprenant = array(
        'name' => 'Liste de tout les apprenants', // title de la page "Apprenants"
        'all_items' => 'Tous les apprenants',  // s'affiché en sous menu
        'singular_name' => 'Apprenant',
        'add_new_item' => 'Nom / Prénom de l\'apprenant', // titre de la page quand on veux ajouter un nouveau apprenant
        'edit_item' => 'Modifier le nom / prénom de l\'apprenant', // titre de la page quand on veux modif un apprenant
        'menu_name' => 'Apprenants' // nom du cpt dans le menu à gauche de wp
    );

    $argsapprenant = array(
        'labels' => $cptapprenant,
        'public' => true,
        'supports' => array( 'title', 'thumbnail', 'excerpt' ),
        'menu_position' => 5,
        'show_in_rest' => true,
    );
    register_post_type( 'apprenant', $argsapprenant );

/////////////////TAXO/////////////////

    // creation de la taxo Ensemble de compétences
    $taxocomp = array(
        'name' => 'Liste des compétences', // titre de la page de la taxo
        'new_item_name' => 'Nom du nouveau Projet',
    	'parent_item' => 'Apprenants', // dans quel cpt la taxo doit se placer
        'menu_name' => 'Ensemble de compétences' // nom de la taxo dans le menu à gauche de wp
    );

    $argscomp = array( 
        'labels' => $taxocomp,
        'public' => true, 
        'show_in_rest' => true,
        'hierarchical' => true, 
    );
    register_taxonomy( 'competences', 'apprenant', $argscomp );


    // création de la taxo Année de la promotion
    $taxopromo = array(
        'name' => 'Liste des promotions', // titre de la page de la taxo
        'new_item_name' => 'Nom du nouveau Projet',
    	'parent_item' => 'Apprenants', // dans quel cpt la taxo doit se placer
        'menu_name' => 'Année de la promotion' // nom de la taxo dans le menu à gauche de wp
    );

    $argspromo = array( 
        'labels' => $taxopromo,
        'public' => true, 
        'show_in_rest' => true,
        'hierarchical' => true, 
    );
    register_taxonomy( 'promotion', 'apprenant', $argspromo );
}

add_action('init', 'monplugin_custom_post_types');

// route qui récup les acf via l'id d'un apprenant  http://localhost/briefapiwordpress/wp-json/monplugin/v1/apprenant/19
add_action('rest_api_init', function(){
    register_rest_route('monplugin/v1', '/apprenant/(?P<id>\d+)', [
        'methods' => 'GET',
        'callback' => function (WP_REST_Request $request) {
            $postID = (int)$request->get_param('id');
            $urllinkedin = (array) get_post_meta($postID, '_url_linkedin');
            $urlportfolio = (array) get_post_meta($postID, '_url_portfolio');
            $urlcv = (array) get_post_meta($postID, '_url_cv');
            $result = array_merge($urllinkedin, $urlportfolio, $urlcv);
            return ($result);
        }
    ]);
});

// Création de mes propre champs acf 
// doc ===> https://wabeo.fr/jouons-avec-les-meta-boxes/
add_action('add_meta_boxes','init_metabox');
function init_metabox(){
  add_meta_box('url_crea', 'Info Perso', 'url_crea', 'apprenant');
}

function url_crea($post){
  $urllinkedin = get_post_meta($post->ID,'_url_linkedin',true);
  $urlportfolio = get_post_meta($post->ID,'_url_portfolio',true);
  $urlcv = get_post_meta($post->ID,'_url_cv',true);
  echo '<label for="url_meta">Lien LinkedIn :</label>';
  echo '<input id="url_meta" type="url" name="url_linkedin" value="'.$urllinkedin.'" />';
  echo '<label for="url_meta">Lien Portfolio :</label>';
  echo '<input id="url_meta" type="url" name="url_portfolio" value="'.$urlportfolio.'" />';
  echo '<label for="url_meta">Lien CV :</label>';
  echo '<input id="url_meta" type="url" name="url_cv" value="'.$urlcv.'" />';
  
}

add_action('save_post','save_metabox');
function save_metabox($post_id){
if(isset($_POST['url_linkedin']))
  update_post_meta($post_id, '_url_linkedin', esc_url($_POST['url_linkedin']));
  if(isset($_POST['url_portfolio']))
  update_post_meta($post_id, '_url_portfolio', esc_url($_POST['url_portfolio']));
  if(isset($_POST['url_cv']))
  update_post_meta($post_id, '_url_cv', esc_url($_POST['url_cv']));
}


// Permet de faire apparaitre les acf et l'img dans http://localhost/briefapiwordpress/wp-json/wp/v2/apprenant?per_page=100
function acf_to_rest_api($response, $post, $request) {
    $urllinkedin = (array) get_post_meta($post->ID, '_url_linkedin');
    $urlportfolio = (array) get_post_meta($post->ID, '_url_portfolio');
    $urlcv = (array) get_post_meta($post->ID, '_url_cv');
    $post_thumbnail = (array) get_img( $post );
    $acf = array_merge($urllinkedin, $urlportfolio, $urlcv, $post_thumbnail);
    $response->data['acf'] = $acf;
    return($response);
}

function get_img( $post = null, $size = 'post-thumbnail' ) {
    $post_thumbnail_id = get_post_thumbnail_id( $post );
 
    if ( ! $post_thumbnail_id ) {
        return false;
    }
 
    return wp_get_attachment_image_url( $post_thumbnail_id, $size );
}

add_filter('rest_prepare_apprenant', 'acf_to_rest_api', 10, 3);

// $curl = curl_init ('http://localhost/briefapiwordpress/wp-json/wp/v2/apprenant/');
// $data = curl_exec($curl);
// if ($data === false) {
//     var_dump(curl_error($curl));
// } else {

// }
// curl_close($curl);